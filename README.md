# Prime multiplication

A program that prints out a multiplication table of the first 10 prime numbers.

The program runs from the command line and print to screen one table.

Across the top and down the left side should be the 10 primes, and the body of the table should contain the product of multiplying these numbers.

## Clone

    git clone git@bitbucket.org:tomas_stefano/prime_multiplication.git

## Setup

Ruby version: 2.1.2

    bundle

## Run Spec

    rspec
    cucumber

# Usage

    ruby -Ilib bin/prime_multiplication    # print multiplication table of the first 10 prime numbers.
    ruby -Ilib bin/prime_multiplication 14 # print multiplication table of the first 14 prime numbers.
