module PrimeMultiplication
  # Generate prime numbers in a simple way.
  #
  class PrimeNumberGenerator
    attr_reader :primes, :maximum

    def initialize(maximum)
      @maximum = maximum
      @primes  = []
      @range   = (0..maximum)
    end

    # @example
    #
    #    PrimeNumberGenerator.new(10).generate
    #    # => [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
    #
    # @return [Array<Fixnum>]
    #
    def generate
      while primes.size < maximum
        primes << @range.select do |element|
          PrimeNumber.new(element).prime?
        end

        primes.flatten!
        @range = (primes.last + 1..primes.last * 2)
      end

      primes.take(maximum)
    end
  end
end
