module PrimeMultiplication
  # Calculate the product of multiplying prime numbers.
  #
  # @example
  #
  #    PrimeProduct.new([2, 3]).multiply_product
  #    # => [[2, 4, 6], [3, 6, 9]]
  #
  class PrimeProduct
    attr_reader :primes

    def initialize(primes)
      @primes    = primes
      @left_line = @primes.dup
    end

    def multiply_product
      @primes.compact.reduce([]) do |line, multiplier|
        multiplied_line = @left_line.map do |prime|
          prime * multiplier
        end

        line << multiplied_line.unshift(multiplier)
      end
    end
  end
end
