module PrimeMultiplication
  # Class respondible just to print the primes in the ruby object format.
  #
  class SimpleReporter < Reporter
    def print
      puts "Primes: #{prime_product.primes.unshift(nil)}"
      print_rows
    end

    def print_rows
      prime_product.multiply_product.each_with_index do |line, index|
        puts "Row ##{index + 1}: #{line}"
      end
    end
  end
end
