require 'terminal-table'

module PrimeMultiplication
  # Class responsible to print the table of multiplied primes.
  #
  class TableReporter < Reporter
    def print
      puts Terminal::Table.new(
        headings: prime_product.primes.unshift(nil),
        rows:     prime_product.multiply_product
      )
    end
  end
end
