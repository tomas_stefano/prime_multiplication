module PrimeMultiplication
  # Class responsible to assign the reporter and print
  #
  class Reporter
    attr_reader :prime_product

    # @param  [Hash] options
    # @option [Reporter] with the reporter class that will be used to print
    # @option [PrimeProduct] prime_product the prime product object
    #
    def self.report(options)
      reporter      = options.fetch(:with)
      prime_product = options.fetch(:prime_product)

      reporter.new(prime_product).print
    end

    def initialize(prime_product)
      @prime_product = prime_product
    end

    def print
      fail NotImplementedError, "implement in #{self}"
    end
  end
end
