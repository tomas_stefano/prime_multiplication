module PrimeMultiplication
  # Verify if the number is prime or not.
  #
  # Follow the rule from the exercise:
  #
  # <b>DO NOT use a library method for Prime (write your own).</b>
  #
  class PrimeNumber
    attr_reader :number

    # Prime number generator that verifies one hundred times
    # that amout passed in the parameter.
    #
    # @return [Array<Fixnum>]
    #
    def self.take(number)
      PrimeNumberGenerator.new(number).generate
    end

    # @param [Fixnum] number the number to be checked if is prime.
    #
    def initialize(number)
      @number = number
    end

    # Fast prime verification version.
    #
    # @return [true, false]
    #
    def prime?
      return true if number.equal? 2

      if number < 2 || number.even?
        false
      else
        square_root = Math.sqrt(number).floor

        3.step(square_root, 2).all? do |step_number|
          (number % step_number).nonzero?
        end
      end
    end
  end
end
