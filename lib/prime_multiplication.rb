require 'prime_multiplication/version'

%w(
  prime_number_generator
  prime_number
  prime_product
  reporters/reporter
  reporters/simple_reporter
  reporters/table_reporter
).each do |file|
  require "prime_multiplication/#{file}"
end
