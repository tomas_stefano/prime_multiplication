# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'prime_multiplication/version'

Gem::Specification.new do |spec|
  spec.name          = 'prime_multiplication'
  spec.version       = PrimeMultiplication::VERSION
  spec.authors       = ['Tomas Stefano']
  spec.email         = ['tomas_stefano@successoft.com']
  spec.summary       = %q(
    Write a program that prints out
    a multiplication table of the first
    10 prime numbers.
  )
  spec.description   = %q(
    The program must run from the command line
    and print to screen 1 table.
    Across the top and down the left side should be
    the 10 primes, and the body of the table should
    contain the product of multiplying these numbers.
  )
  spec.homepage      = ''
  spec.license       = 'MIT'

  spec.files         = `git ls-files`.split('\x0')
  spec.executables   = spec.files.grep(/^bin/) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(/^(test|spec|features)/)
  spec.require_paths = ['lib']

  spec.add_dependency 'terminal-table', '1.4.5'

  spec.add_development_dependency 'bundler', '~> 1.6'
  spec.add_development_dependency 'rake'
  spec.add_development_dependency 'rspec', '~> 3.0'
  spec.add_development_dependency 'aruba'
  spec.add_development_dependency 'cucumber'
  spec.add_development_dependency 'yard'
end
