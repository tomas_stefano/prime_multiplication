require 'spec_helper'

RSpec.describe PrimeMultiplication::PrimeNumberGenerator do
  subject(:generator) do
    described_class.new(maximum).generate
  end

  describe '#generate' do
    context 'when zero' do
      let(:maximum) { 0 }

      it 'returns an empty array' do
        expect(generator).to eq([])
      end
    end

    context 'when greater than zero' do
      let(:maximum) { 20 }

      it 'returns the same amout of prime numbers requested' do
        expect(generator).to eq([
          2, 3, 5, 7, 11, 13, 17, 19, 23, 29,
          31, 37, 41, 43, 47, 53, 59, 61, 67, 71
        ])
      end
    end
  end
end


