require 'spec_helper'

RSpec.describe PrimeMultiplication::PrimeNumber do
  describe '.take' do
    let(:generator_class) { PrimeMultiplication::PrimeNumberGenerator }
    let(:generator) { double }

    it 'call the prime number generator' do
      expect(generator_class).to receive(:new).with(10).and_return(generator)
      expect(generator).to receive(:generate)
      described_class.take(10)
    end
  end

  describe '#prime?' do
    context 'when is prime' do
      subject(:primes) do
        [2, 3, 5, 7, 11, 13, 17, 19, 23, 29]
      end

      it 'returns true' do
        primes.each do |prime|
          expect(described_class.new(prime)).to be_prime
        end
      end
    end

    context 'when is not prime' do
      subject(:numbers) do
        [4, 6, 8, 10, 12, 15, 21, 27, 35]
      end

      it 'returns false' do
        numbers.each do |number|
          expect(described_class.new(number)).to_not be_prime
        end
      end
    end
  end
end