require 'spec_helper'

RSpec.describe PrimeMultiplication::PrimeProduct do
  subject(:prime_product) do
    described_class.new(primes)
  end

  describe '#primes' do
    let(:primes) { [2, 3, 5] }

    it 'returns the assign primes' do
      expect(prime_product.primes).to eq([2, 3, 5])
    end
  end

  describe '#multiply_product' do
    context 'when receives empty array' do
      let(:primes) { [] }

      it 'returns an empty array' do
        expect(prime_product.multiply_product).to eq([])
      end
    end

    context 'when receives the array only with nil values' do
      let(:primes) { [nil] }

      it 'returns an empty array' do
        expect(prime_product.multiply_product).to eq([])
      end
    end

    context 'when receives the prime numbers' do
      let(:primes) { [2, 3, 5] }

      it 'returns all the multiplication with the primes itself' do
        expect(prime_product.multiply_product).to eq([
          [2, 4, 6, 10], [3, 6, 9, 15], [5, 10, 15, 25]
        ])
      end
    end
  end
end
