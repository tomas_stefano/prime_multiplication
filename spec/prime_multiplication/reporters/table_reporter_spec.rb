require 'spec_helper'

RSpec.describe PrimeMultiplication::TableReporter do
  let(:prime_product) do
    double(
      primes: [2, 3, 5],
      multiply_product: [[2, 4, 6], [3, 6, 9]]
    )
  end
  subject(:reporter) { described_class.new(prime_product) }
  it_behaves_like 'a reporter'

  describe '#print' do
    let(:terminal_table) { double }

    it 'assigns the terminal table and passing the headings and the rows' do
      expect(Terminal::Table).to receive(:new).with(
        headings: [nil, 2, 3, 5],
        rows:     [[2, 4, 6], [3, 6, 9]]
      ).and_return(terminal_table)
      expect(reporter).to receive(:puts).with(terminal_table)

      reporter.print
    end
  end
end