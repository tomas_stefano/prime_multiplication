require 'spec_helper'

RSpec.describe PrimeMultiplication::SimpleReporter do
  let(:prime_product) do
    double(
      primes: [2, 3, 5],
      multiply_product: [[2, 4, 6], [3, 6, 9]]
    )
  end
  subject(:reporter) { described_class.new(prime_product) }
  it_behaves_like 'a reporter'

  describe '#print' do
    it 'assigns the terminal table and passing the headings and the rows' do
      expect(reporter).to receive(:puts).with("Primes: [nil, 2, 3, 5]")
      expect(reporter).to receive(:print_rows)

      reporter.print
    end
  end
end