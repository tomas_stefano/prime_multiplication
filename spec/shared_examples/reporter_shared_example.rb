RSpec.shared_examples 'a reporter' do
  describe '#initialize' do
    let(:prime_product) { double }

    it 'receives the prime product' do
      expect {
        described_class.new(prime_product)
      }.to_not raise_error
    end
  end

  describe '#prime_product' do
    it 'receives the prime product' do
      expect(subject).to respond_to(:prime_product)
    end
  end

  describe '#print' do
    it 'responds to print' do
      expect(subject).to respond_to(:print)
    end
  end
end